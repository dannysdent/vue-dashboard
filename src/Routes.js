import Vue from 'vue';
import Router from 'vue-router';

import Layout from '@/components/Layout/Layout';
import LayoutMain from '@/components/LayoutMain/LayoutMain';
import Login from '@/pages/Login/Login';
import ErrorPage from '@/pages/Error/Error';
// Core
import TypographyPage from '@/pages/Typography/Typography';

// Tables
import TablesBasicPage from '@/pages/Tables/Basic';

// Maps
import GoogleMapPage from '@/pages/Maps/Google';

// Default Dashboard
import AnalyticsPage from '@/pages/Dashboard/Dashboard';

// Main Dashboard
import DashboardPage from '@/pages/DashboardMain/DashboardMain';

// Charts
import ChartsPage from '@/pages/Charts/Charts';

// Start To Build
import StartToBuildPage from '@/pages/Applications/start-to-build/start-to-build';

// Annual Service Fee
import AnnualServiceFeePage from '@/pages/Applications/annual-service-fee/annual-service-fee';

// Apply For Inspection
import ApplyForInspectionPage from '@/pages/Applications/apply-for-inspection/apply-for-inspection';

// Insurance
import InsurancePage from '@/pages/Applications/insurance/insurance';

// Quotations
import QuotationsPage from '@/pages/Applications/quotations/quotations';

// Payments
import PaymentsPage from '@/pages/Payments/Payments';

// Ui
import IconsPage from '@/pages/Icons/Icons';
import NotificationsPage from '@/pages/Notifications/Notifications';


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/error',
      name: 'Error',
      component: ErrorPage,
    },
    {
      path: '/app',
      name: 'Layout',
      component: Layout,
      children: [
        {
          path: 'dashboard',
          name: 'AnalyticsPage',
          component: AnalyticsPage,
        },
        // {
        //   path: 'dashboardmain',
        //   name: 'DashboardPage',
        //   component: DashboardPage,
        // },
        {
          path: 'typography',
          name: 'TypographyPage',
          component: TypographyPage,
        },
        {
          path: 'start-to-build',
          name: 'StartToBuildPage',
          component: StartToBuildPage,
        },
        {
          path: 'quotations',
          name: 'QuotationsPage',
          component: QuotationsPage,
        },
        {
          path: 'insurance',
          name: 'InsurancePage',
          component: InsurancePage,
        },
        {
          path: 'apply-for-inspection',
          name: 'ApplyForInspectionPage',
          component: ApplyForInspectionPage,
        },
        {
          path: 'annual-service-fee',
          name: 'AnnualServiceFeePage',
          component: AnnualServiceFeePage,
        },
        {
          path: 'components/icons',
          name: 'IconsPage',
          component: IconsPage,
        },
        {
          path: 'reports',
          name: 'NotificationsPage',
          component: NotificationsPage,
        },
        {
          path: 'statistics',
          name: 'ChartsPage',
          component: ChartsPage,
        },
        {
          path: 'payments',
          name: 'PaymentsPage',
          component: PaymentsPage,
        },
        {
          path: 'tables',
          name: 'TablesBasicPage',
          component: TablesBasicPage,
        },
        {
          path: 'components/maps',
          name: 'GoogleMapPage',
          component: GoogleMapPage,
        },
      ],
    },
    {
      path: '/main',
      name: 'LayoutMain',
      component: LayoutMain,
      children: [
        {
          path: 'dashboard',
          name: 'DashboardPage',
          component: DashboardPage,
        }
      ],
    },
  ],
});
