export default {
  bigStat: [
    {
      product: 'Light Blue',
      total: '4,232',
      color: 'primary',
      registrations: {
        value: 830,
        profit: true,
      },
      bounce: {
        value: 4.5,
        profit: false,
      },
    },
    {
      product: 'Sing App',
      total: '754',
      color: 'danger',
      registrations: {
        value: 30,
        profit: true,
      },
      bounce: {
        value: 2.5,
        profit: true,
      },
    },
    {
      product: 'RNS',
      total: '1,025',
      color: 'info',
      registrations: {
        value: 230,
        profit: true,
      },
      bounce: {
        value: 21.5,
        profit: false,
      },
    },
  ],
  table: [
    {
      id: 0,
      refno: '225678GHY',
      applicant: 'Mark Otto',
      buildingtype: 'ViewApartments',
      block: '25',
      dimension: '23*34',
      startdate: '4 Jun 2017',
      estimatedcompletion: '4 Oct 2017',
    },
    {
      id: 1,
      refno: '67837YUDG',
      applicant: 'Catherine Magdaline',
      buildingtype: 'Tinegi Apartments',
      block: '5',
      dimension: '28*34',
      startdate: '4 Jun 2027',
      estimatedcompletion: '4 Oct 2034',
    },
  ],
};
