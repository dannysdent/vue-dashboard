export default {
  bigStat: [
    {
      product: 'Light Blue',
      total: '4,232',
      color: 'primary',
      registrations: {
        value: 830,
        profit: true,
      },
      bounce: {
        value: 4.5,
        profit: false,
      },
    },
    {
      product: 'Sing App',
      total: '754',
      color: 'danger',
      registrations: {
        value: 30,
        profit: true,
      },
      bounce: {
        value: 2.5,
        profit: true,
      },
    },
    {
      product: 'RNS',
      total: '1,025',
      color: 'info',
      registrations: {
        value: 230,
        profit: true,
      },
      bounce: {
        value: 21.5,
        profit: false,
      },
    },
  ],
  table: [
    {
      id: 0,
      refno: '78837HCNDU',
      type: 'Apartment',
      date: '11 May 2017',
      trnsactionid: 'XH89998MX',
      amount: '27000',
      service: 'Rent',
      applicantname: 'Joel Kinuthia',
      buildingrefno: 'NXNHH89',
    },
    {
      id: 1,
      refno: '78837HCNDU',
      type: 'Apartment',
      date: '01 Jun 2018',
      trnsactionid: 'HH89698MX',
      amount: '71000',
      service: 'Rent',
      applicantname: 'Keith Ken',
      buildingrefno: 'NXNHH89',
    },
  ],
};
