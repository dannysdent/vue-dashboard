# ERP Dashboard

#### 1. Run `yarn install`

This will install both run-time project dependencies and developer tools listed
in [package.json](../package.json) file.

#### 2. Run `yarn build`

This command will build the app from the source files (`/src`) into the output
`/dist` folder. Then open `dist/index.html` in your browser.


#### 3. Run `npm run serve`
This command will watch for changes in `/src` and recompile vue templates & scss styles on the fly.


